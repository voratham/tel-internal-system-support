import UserRepository from '../../models/user/user.repository'

export default async () => {
  const respUser = await UserRepository.findAll(
    {},
    {
      raw: true,
    },
  )
  console.log('listall ::', respUser)
  return respUser
}
