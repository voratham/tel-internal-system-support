import UserRepository from '../../models/user/user.repository'
import { hashPassword, generateJwtToken } from '../../utils/domain'

export default async body => {
  try {
    const findUsername = await UserRepository.findOne({
      where: {
        username: body.username,
      },
    })
    const findUsereMail = await UserRepository.findOne({
      where: {
        email: body.email,
      },
    })
    if (findUsername || findUsereMail) {
      throw {
        tag: 'register',
        msg: 'Username or email existing !',
      }
    }
    const userHashPassword = await hashPassword(body.password)
    const newData = {
      ...body,
      password: userHashPassword,
    }
    console.log('newData :: ', newData)
    const newUser = await UserRepository.create(newData)
    const token = generateJwtToken({
      username: newUser.username,
      roles: newUser.roles,
    })
    return token
  } catch (error) {
    console.log('error :: ', error)
    return error
  }
}
