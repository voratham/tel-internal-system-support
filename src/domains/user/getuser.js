import UserRepository from '../../models/user/user.repository'

export default async getUser => {
  const getUserInfo = await UserRepository.findOne({
    where: {
      username: getUser,
    },
    attributes: ['username', 'name', 'position', 'email', 'phone', 'roles'],
  })
  console.log('listUser :: ', getUserInfo)
  return getUserInfo
}
