import UserRepository from '../../models/user/user.repository'
import { generateJwtToken, comparePassword } from '../../utils/domain'

export default async body => {
  try {
    const authlogin = await UserRepository.findOne({
      where: {
        username: body.username,
      },
    })
    if (!authlogin) throw new Error('Authlogin')

    const validPassword = await comparePassword(body.password, authlogin.password)
    if (validPassword) {
      const token = await generateJwtToken({
        username: authlogin.username,
        role: authlogin.roles,
      })
      console.log('token :: ', token)
      return {
        token,
      }
    }
  } catch (error) {
    return error
  }
}
