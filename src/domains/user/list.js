import UserRepository from '../../models/user/user.repository'

export default async () => {
  const listUser = await UserRepository.findAll({})
  console.log('listUser :: ', listUser)
  return listUser
}
