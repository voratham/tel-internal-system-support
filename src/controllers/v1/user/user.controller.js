import { HttpMethod, route } from '@spksoft/koa-decorator'
import listUser from '../../../domains/user/list'
import loginUser from '../../../domains/user/login'
import verifyToken from '../../../middlewares/verifytoken'
import ListGetAll from '../../../domains/user/listall'
import createUser from '../../../domains/user/register'
import getUserInfo from '../../../domains/user/getuser'
import comparelogin from '../../../middlewares/comparelogin'

@route('/v1/users')
export default class UserController {
  @route('/', HttpMethod.GET)
  async listUser(ctx) {
    const respData = await listUser()
    ctx.body = {
      status: true,
      data: respData,
      time: Date.now(),
    }
  }

  @route('/listgetall', HttpMethod.GET, verifyToken)
  async ListGetAll(ctx) {
    const respData = await ListGetAll()
    ctx.body = {
      status: true,
      data: respData,
    }
  }

  @route('/login', HttpMethod.POST)
  async loginUser(ctx) {
    const { body } = ctx.request
    const respData = await loginUser(body)
    ctx.body = {
      status: true,
      data: {
        ...respData,
      },
    }
  }

  @route('/register', HttpMethod.POST)
  async createUser(ctx) {
    const { body } = ctx.request
    const respdata = await createUser(body)
    ctx.body = {
      status: true,
      data: respdata,
    }
  }

  @route('/info/:username', HttpMethod.get, verifyToken)
  async getUserInfo(ctx) {
    const getUser = ctx.params.username
    const respdata = await getUserInfo(getUser)
    ctx.body = {
      status: true,
      data: respdata,
    }
  }
}
