import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

export const hashPassword = (password: string) =>
  new Promise(resolve => {
    bcrypt.hash(password, 10).then(res => {
      resolve(res)
    })
  })
export const generateJwtToken = async user =>
  await jwt.sign(user, process.env.SECRETKEY, {
    expiresIn: '1hr',
  })

export const comparePassword = (password: string, hashPassword: string) =>
  new Promise(resolve => {
    bcrypt.compare(password, hashPassword).then(res => {
      resolve(res)
    })
  })
