import UserRepository from '../models/user/user.repository'

export default async (ctx, next) => {
  const { body } = ctx.request
  const authlogin = await UserRepository.findOne({
    where: {
      username: body.username,
    },
  })
  if (authlogin) {
    await next()
  } else {
    ctx.body = {
      msg: 'username is incorrect',
    }
  }
}
