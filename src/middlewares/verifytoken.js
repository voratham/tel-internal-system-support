import jwt from 'jsonwebtoken'

export default async (ctx, next) => {
  const {
    header: { authorization },
  } = ctx.request
  await jwt.verify(authorization, process.env.SECRETKEY, async (err, decoded) => {
    if (decoded) {
      await next()
    } else {
      ctx.body = {
        status: false,
        msg: 'no authorization',
      }
    }
  })
}
