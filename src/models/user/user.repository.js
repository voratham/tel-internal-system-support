import Sequelize from 'sequelize'
import sequelize from '../../libraries/database/client/sequelize'

const schema = {
  name: Sequelize.STRING,
  username: {
    type: Sequelize.STRING,
    unique: true,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
  },
  password: Sequelize.STRING,
  phone: Sequelize.STRING,
  position: Sequelize.STRING,
  roles: {
    type: Sequelize.ENUM,
    values: ['admin', 'guest', 'test'],
  },
}

const UserRepository = sequelize.define('user', schema)
export default UserRepository
